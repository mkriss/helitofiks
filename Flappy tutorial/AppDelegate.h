//
//  AppDelegate.h
//  Flappy tutorial
//
//  Created by Krišs Miķelsons on 25/02/14.
//  Copyright (c) 2014 Krišs Miķelsons. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
